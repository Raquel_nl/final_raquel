<?= $this->extend('layout/template') ?>
<?= $this->section('content') ?>

<?php if (!$auth->loggedIn() AND $auth->isAdmin()): ?>
<?php $user = $auth->user()->row(); ?>
<div><a href="<?= site_url('auth/login'); ?>">Entrar</a></div>
<?php endif ?>

<div class="d-flex flex-row-reverse bd-highlight">
   <?= $user->first_name . ' ' . $user->last_name ?>
</div>

<?php if ($auth->loggedIn() AND $auth->isAdmin()): ?>
<?php $user = $auth->user()->row(); ?>
<div><a href="<?= site_url('auth/logout'); ?>">Salir</a></div>

<?php if (! empty($familias) && is_array($familias)) : ?>
<img src="<?php echo base_url("assets/iconostienda".str_replace($familias).".png");?>">


<div><br><button type="button" class="btn btn-light">ORTÉSICA</button></div>
<div><br><button type="button" class="btn btn-light">CALZADO</button></div>
<div><br><button type="button" class="btn btn-light">MASTECTOMÍA</button></div>
<div><br><button type="button" class="btn btn-light">AYUDAS TÉCNICAS</button></div>
<div><br><button type="button" class="btn btn-light">PROTÉSICA</button></div>
<div><br><button type="button" class="btn btn-light">SUMINISTROS</button></div>
<?php endif ?>

<?php else : ?>
     <h3>No estas logueado</h3>
    <p>Inicia sesión para ver el contenido</p>
<?php endif ?>


<?= $this->endSection() ?>

