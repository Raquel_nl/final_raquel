<?= $this->extend('layout/template') ?>
<?= $this->section('content') ?>

<?php
    $auth = new \IonAuth\Libraries\IonAuth();
?>


<div><a href="<?= site_url('auth/login'); ?>">Entrar</a></div>




<?php if ($auth->loggedIn() AND $auth->isAdmin()): ?>
<?php $user = $auth->user()->row(); ?>
<div><a href="<?= site_url('auth/logout'); ?>">Salir</a></div>
<table class="table table-striped">
            <thead>
            <th>Código</th>
            <th>Producto</th>
            <th>Tallas</th>
            </thead>
            <?php foreach ($productos as $producto): ?>
                <tr>
            <td><?= $producto['CodigoProducto'] ?></td>
            <td><?= $producto['Nombre'] ?></td>
            <td><?= $producto['Talla'] ?></td>
            <?php if ($auth->loggedIn() AND $auth->isAdmin()): ?>
            <td><a href="<?= site_url('home/borrar/'.$productos['CodigoProducto'])?>" 
                   class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar la solicitud de <?= $solicitud['solicitante'] ?>')">Borrar</a>
            <?php endif ?>
            </td>
               </tr>
            <?php endforeach; ?> 
        </table>

<?php else : ?>
     <h3>No estas logueado</h3>
    <p>Inicia sesión para ver el contenido</p>
<?php endif ?>

<?= $this->endSection() ?> 