<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Signin Template for Ion Auth</title>
    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">    
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
  </head>
  <body class="text-center login" >
    <div class='form-signin'>
      <h1><?php echo lang('Auth.login_heading');?></h1>
      <p><?php echo lang('Auth.login_subheading');?></p>
      <div id="infoMessage"><?php echo $message;?></div>     
      <?php echo form_open('auth/login');?>
        <p>
          <?php echo form_label(lang('Auth.login_identity_label'), 'identity');?>
          <?php echo form_input($identity,set_value('identity'),['class'=>'form-control', 'required'=>'required', 'autofocus'=>'autofocus']);?>
        </p>
        <p>
          <?php echo form_label(lang('Auth.login_password_label'), 'password');?>
          <?php echo form_input($password, 'password', ['class'=>'form-control', 'required'=>'required']);?>
        </p>
        <p><?php echo form_submit('submit', lang('Auth.login_submit_btn'),['class'=>'btn btn-lg btn-primary btn-block']);?></p>
      <?php echo form_close();?>
      <p><a href="forgot_password"><?php echo lang('Auth.login_forgot_password');?></a></p>
      </div>      
  </body>
</html>
