<?php

namespace App\Controllers;
use App\Models\FamiliaModel;
use App\Models\ProductosModel;

use Config\Services;


class Home extends BaseController
{
    protected $auth;
    protected $session;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        //------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //------------------------------------------------------------
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();

    }

    public function catalogo()
    {   $data['titol'] = "Catalogo";
        $FamiliaModel = new FamiliaModel();
        $data['familias'] = $FamiliaModel    ->select ("productos.CodigoProducto, productos.CodigoFamilia")
                                             ->join('familias' , 'familias.CodigoFamilia = productos.CodigoFamilia' , 'LEFT')
                                             ->count ($data)
                                             ->findAll();
    
     return view('articulos/catalogoview',$data);
     
    }
    
     public function productos(){  
    $data['titol'] = "Catalogo de Productos";
    $ProductosModel = new ProductosModel();
    $data['productos'] = $ProductosModel ->select  ("productos.CodigoProducto, productos.Nombre, productos.Talla")
                                         ->join ('lineas', 'lineas.CodigoLinea = productos.CodigoLinea', 'LEFT')
                                         ->findAll();
    return view('articulos/productosview', $data);
}
    
    public function index()
    {
        if (!$this->auth->loggedIn()){
            return redirect()->to('auth/login');
        }
        return view('welcome_message');
    }
    

    public function borrar()
    {
   if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
        $FamiliaModel = new FamiliaModel();
        $FamiliaModel->delete($CodigoProducto);
        return redirect()->to('Home/catalogo');
        } else {
            echo "No puedes borrar porque tienes permisos insuficientes";
           }
    endif;  