<?php

namespace App\Models;
use CodeIgniter\Model;

class ProductosModel extends Model{
     protected $table = 'productos';
    protected $primaryKey = 'CodigoProducto';
    protected $returnType = 'array';
}
