<?php

namespace App\Models;
use CodeIgniter\Model;

class FamiliaModel extends Model {
    protected $table = 'familias';
    protected $primaryKey = 'CodigoFamilias';
    protected $returnType = 'array';
}

